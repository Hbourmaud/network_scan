# TP5 : Network Scanner

> scan_network.py is a python script to find devices on your connected networks and see their different open ports, and more ...

## How to use

1. clone or download the project
2. Install the different python libraries needed :
    1. **Scapy:**  ``pip install scapy`` https://pypi.org/project/scapy/

### How to run

1. run: `sudo python scan_network.py`
2. input the index network to scan: e.g.
```
Source  Index  Name    MAC                IPv4       IPv6
sys     1      lo      00:00:00:00:00:00  127.0.0.1  ::1
sys     2      enp0s3  08:00:00:a8:a8:c8  10.3.1.5   fe80::a00:a00:fea8:a8c8
Choose index : 2
```
This example will scan the network of the second network card : *enp0s3*

### Results
```
Available devices in the network:
IP                  MAC
10.3.1.1            0a:00:27:00:00:09
Port:  135 Name:  epmap Status: Open
Port:  139 Name:  netbios-ssn Status: Open
Port:  53 Name:  domain Status: Open/Filtered
10.3.1.11           08:00:00:81:7f:c8
Port:  22 Name:  ssh Status: Open
Port:  53 Name:  domain Status: Open/Filtered
```
So you can see the differents devices on the network follows by the open ports of the device. (Discoverable by TCP/UDP)  

By default the last discovered devices are saved in the folder of the script in `file.json`  

**NB:** Not all ports are scan by default.

### Arguments

After `sudo python scan_network.py` you can add some arguments:  

1. `--help`
    Find some help with arguments and more.
    ```
    This is a scan network script, you can choose in first the network to scan.
    A general list of ports are scan by default, but you can change this by add some args:
    -h | --help | help : Print this information
    1 argument : the port number you want to search
    2 arguments : to use a range port, the first and the second argument must be a port number
    -s : Use list of devices on json save file at file.json (same folder of script)
    ```
    
2. n (Port Number)
    Scan a specific port.
    
3. n y (Two Port Numbers)
    Scan a port range between the two port numbers given.
    
4. -s
    Use the last saved devices list on file.json
    
    
    
*This project are made by [Brandon](https://git.ytrack.learn.ynov.com/BPORTE1) and [Hugo](https://git.ytrack.learn.ynov.com/HBOURMAUD)*