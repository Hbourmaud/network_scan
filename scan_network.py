from scapy.all import ARP,Ether,srp,IFACES,sr,IP,TCP,UDP,ICMP,get_if_addr
from socket import getservbyport, socket
from pathlib import Path
import sys, re, json, fcntl, struct, socket

def lastPrint(res, prot):
    if(prot == "TCP"):
        for sent,received in res:
            if(received[1].flags==18) and (received[1].haslayer(TCP)): #18 == synACK
                print('Port: ',received[1].sport, 'Name: ',getservbyport(received[1].sport), 'Status: Open')
    else:
        for sent,received in res:
            if(received != None ) and (len(received)>0):
                if received[0].haslayer(UDP):
                    print('Port: ',received[0].dport, 'Name: ',getservbyport(received[0].dport), 'Status: Open/Filtered')
                elif(int(received[0].getlayer(ICMP).type)==3 and int(received[0].getlayer(ICMP).code) in [1,2,9,10,13]):
                    print('Port: ',received[0].dport, 'Name: ',getservbyport(received[0].dport), 'Status: Open/Filtered')

clients = []

if (len(sys.argv) > 1):
    if (sys.argv[1] == 'help' or sys.argv[1] == '-h' or sys.argv[1] == '--help'):
        print('This is a scan network script, you can choose in first the network to scan.')
        print('A general list of ports are scan by default, but you can change this by add some args:')
        print('-h | --help | help : Print this information')
        print('1 argument : the port number you want to search')
        print('2 arguments : to use a range port, the first and the second argument must be a port number')
        print('-s Use list of devices on json save file at file.json (same folder of script) ')
        sys.exit()

    elif (sys.argv[1] == '-s'):
        try:
            with open('file.json', 'r') as json_file:
                data = json.load(json_file)
                clients = data      
            json_file.close()
        except:
            print("No file.json find in repository. Run program withour -s or create manually the file")
            sys.exit()

    elif (re.search("[a-zA-Z]", sys.argv[1])):
        print('the argument must be only a port number')
        sys.exit()

    elif (int(sys.argv[1]) < 0 or int(sys.argv[1]) > 65535):
        print('the argument must be only a port number')
        sys.exit()

if (clients == []):
    IFACES.show()
    id = input("Choose index : ")
    try:
        ip_target = ((get_if_addr(IFACES.dev_from_index(id))))
    except:
        print("Error with the chosen value!")
        sys.exit()
    interf_name = (IFACES.dev_from_index(id))
    s = struct.Struct('256s')
    netmask = (socket.inet_ntoa(fcntl.ioctl(socket.socket(socket.AF_INET, socket.SOCK_DGRAM), 35099, s.pack(str(interf_name).encode('ascii')))[20:24]))
    netmask = sum([bin(int(x)).count('1') for x in netmask.split('.')])
    ip_target += "/"+str(netmask)
    arp_request = ARP(pdst = ip_target)
    broadcast = Ether(dst ="ff:ff:ff:ff:ff:ff")
    arp_request_broadcast = broadcast / arp_request
    answered_list = srp(arp_request_broadcast,timeout=3,verbose=0, iface=IFACES.dev_from_index(id))[0]
    for sent, received in answered_list:
        #for each response, append ip and mac address to clients list
        clients.append({'ip': received.psrc, 'mac': received.hwsrc})

print("Available devices in the network:")
print("IP" + " "*18+"MAC")

filename = Path('./file.json')
filename.touch(exist_ok=True)

with open('file.json', 'w', encoding='utf-8') as f:
    json.dump(clients, f, ensure_ascii=False, indent=4)

prt = None

if (len(sys.argv) == 1):
    prt = None

elif (sys.argv[1] == '-s'):
    prt = None

else :
    prt = int(sys.argv[1])

for client in clients:
    print("{:16}    {}".format(client['ip'], client['mac']))

    if (prt == None):
        res, unans = sr( IP(dst=client['ip'])/TCP(flags="S", dport=[22,53,80,135,139,443,2049]),timeout=2,verbose=0 )
        lastPrint(res,"TCP")
        res.clear()
        res.append(sr( IP(dst=client['ip'])/UDP(dport=[53,67,443]),timeout=2,verbose=0))
        lastPrint(res,"UDP")
    elif(len(sys.argv)>2):
        if (sys.argv[1] != None and sys.argv[2] != None):
            for p in range (int(sys.argv[1]),int(sys.argv[2]), 1):
                res, unans = sr( IP(dst=client['ip'])/TCP(flags="S", dport=[p]),timeout=2,verbose=0 )
                lastPrint(res,"TCP")
    else:
        res, unans = sr( IP(dst=client['ip'])/TCP(flags="S", dport=[prt]),timeout=2,verbose=0 )
        lastPrint(res,"TCP")
        res.clear()
        res.append(sr( IP(dst=client['ip'])/UDP(dport=[prt]),timeout=2,verbose=0))
        lastPrint(res,"UDP")